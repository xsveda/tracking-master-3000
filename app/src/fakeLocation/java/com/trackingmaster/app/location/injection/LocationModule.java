package com.trackingmaster.app.location.injection;

import android.content.Context;

import com.trackingmaster.app.location.application.LocationController;
import com.trackingmaster.lib.location.LocationClient;
import com.trackingmaster.lib.location.LocationProvider;
import com.trackingmaster.lib.location.provider.fake.FakeLocationProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
@Module
public final class LocationModule {

    public LocationModule(Context context) {
    }

    @Singleton
    @Provides
    LocationProvider provideLocationProvider() {
        return new FakeLocationProvider();
    }

    @Singleton
    @Provides
    LocationClient provideLocationClient(LocationProvider locationProvider) {
        return new LocationClient(locationProvider);
    }

    @Singleton
    @Provides
    LocationController provideLocationController(LocationClient locationClient) {
        return new LocationController(locationClient);
    }
}
