package com.trackingmaster.app;

import android.app.Application;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public final class TrackingApplication extends Application {

    private static TrackingApplication instance;

    /**
     * Static application instance server only for {@link ApplicationComponent}
     */
    static TrackingApplication get() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
