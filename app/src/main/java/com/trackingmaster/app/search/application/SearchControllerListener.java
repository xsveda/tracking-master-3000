package com.trackingmaster.app.search.application;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public interface SearchControllerListener {

    void onSearchFinished();
}
