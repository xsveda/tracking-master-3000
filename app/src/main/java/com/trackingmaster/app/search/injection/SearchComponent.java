package com.trackingmaster.app.search.injection;

import com.trackingmaster.app.search.ui.SearchFragment;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public interface SearchComponent {
    void inject(SearchFragment fragment);
}
