package com.trackingmaster.app.search.application;

import com.google.common.base.Optional;
import com.trackingmaster.app.search.domain.SearchUsecase;
import com.trackingmaster.app.search.domain.SearchUsecaseListener;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Provider;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public final class SearchController implements SearchUsecaseListener {

    private final Provider<SearchUsecase> searchUsecaseProvider;
    private final Set<SearchControllerListener> listeners = new HashSet<>();

    private Optional<SearchUsecase> runningSearch = Optional.absent();
    private Optional<String> lastQuery = Optional.absent();
    private Optional<String> lastResult = Optional.absent();

    public SearchController(Provider<SearchUsecase> searchUsecaseProvider) {
        this.searchUsecaseProvider = checkNotNull(searchUsecaseProvider);
    }

    public void register(SearchControllerListener listener) {
        listeners.add(checkNotNull(listener));
    }

    public void unregister(SearchControllerListener listener) {
        listeners.remove(checkNotNull(listener));
    }

    public void search(String searchQuery) {
        cancelRunningSearch();

        lastQuery = Optional.of(searchQuery);
        lastResult = Optional.absent();

        executeNewSearch(searchQuery);
    }

    public boolean isRunning() {
        return runningSearch.isPresent();
    }

    public Optional<String> getLastQuery() {
        return lastQuery;
    }

    public Optional<String> getLastResult() {
        return lastResult;
    }

    @Override
    public void onSearchResult(String result) {
        lastResult = Optional.of(result);
        runningSearch = Optional.absent();
        notifyRegisteredListeners();
    }

    private void executeNewSearch(String searchQuery) {
        SearchUsecase usecase = searchUsecaseProvider.get();
        usecase.execute(searchQuery, this);
        runningSearch = Optional.of(usecase);
    }

    private void cancelRunningSearch() {
        if (runningSearch.isPresent()) {
            runningSearch.get().cancel();
            runningSearch = Optional.absent();
        }
    }

    private void notifyRegisteredListeners() {
        for (SearchControllerListener listener : listeners) {
            listener.onSearchFinished();
        }
    }
}
