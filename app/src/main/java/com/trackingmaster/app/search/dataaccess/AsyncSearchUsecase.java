package com.trackingmaster.app.search.dataaccess;

import android.os.AsyncTask;

import com.google.common.base.Optional;
import com.trackingmaster.app.search.domain.SearchUsecase;
import com.trackingmaster.app.search.domain.SearchUsecaseListener;
import com.trackingmaster.lib.search.SearchClient;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public final class AsyncSearchUsecase implements SearchUsecase {

    private final SearchClient searchClient;
    private Optional<AsyncTask<Void, Void, String>> currentTask = Optional.absent();

    public AsyncSearchUsecase(SearchClient searchClient) {
        this.searchClient = checkNotNull(searchClient);
    }

    @Override
    public void execute(final String searchQuery, final SearchUsecaseListener listener) {
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                return searchClient.search(searchQuery);
            }

            @Override
            protected void onPostExecute(String result) {
                if (!isCancelled()) {
                    listener.onSearchResult(result);
                }
            }
        };
        task.execute();
        currentTask = Optional.of(task);
    }

    @Override
    public void cancel() {
        if (currentTask.isPresent()) {
            currentTask.get().cancel(true);
            currentTask = Optional.absent();
        }
    }
}
