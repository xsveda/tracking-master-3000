package com.trackingmaster.app.search.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.trackingmaster.app.ApplicationComponent;
import com.trackingmaster.app.R;
import com.trackingmaster.app.search.application.SearchController;
import com.trackingmaster.app.search.application.SearchControllerListener;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public final class SearchFragment extends Fragment implements SearchControllerListener {

    @Inject SearchController searchController;

    @BindView(R.id.query) EditText queryField;
    @BindView(R.id.progress) ProgressBar progressBar;
    @BindView(R.id.result_container) ViewGroup resultContainer;
    @BindView(R.id.result) TextView resultLabel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApplicationComponent.get().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.search_fragment, container);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        ButterKnife.bind(this, view);
        updateView();
    }

    @Override
    public void onStart() {
        super.onStart();
        searchController.register(this);
    }

    @Override
    public void onStop() {
        searchController.unregister(this);
        super.onStop();
    }

    @OnClick(R.id.search)
    void onSearchClicked() {
        searchController.search(queryField.getText().toString());
        updateView();
    }

    @Override
    public void onSearchFinished() {
        updateView();
    }

    private void updateView() {
        progressBar.setVisibility(searchController.isRunning() ? VISIBLE : INVISIBLE);

        queryField.setText(searchController.getLastQuery().orNull());

        String result = searchController.getLastResult().orNull();
        if (TextUtils.isEmpty(result)) {
            resultContainer.setVisibility(INVISIBLE);
        } else {
            resultContainer.setVisibility(VISIBLE);
            resultLabel.setText(result);
        }
    }
}
