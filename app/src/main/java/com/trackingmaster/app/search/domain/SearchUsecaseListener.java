package com.trackingmaster.app.search.domain;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public interface SearchUsecaseListener {

    void onSearchResult(String result);
}
