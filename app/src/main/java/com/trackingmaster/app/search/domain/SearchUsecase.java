package com.trackingmaster.app.search.domain;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public interface SearchUsecase {

    void execute(String searchQuery, SearchUsecaseListener listeners);

    void cancel();
}
