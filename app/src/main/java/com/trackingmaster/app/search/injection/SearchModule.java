package com.trackingmaster.app.search.injection;

import com.trackingmaster.app.search.application.SearchController;
import com.trackingmaster.app.search.dataaccess.AsyncSearchUsecase;
import com.trackingmaster.app.search.domain.SearchUsecase;
import com.trackingmaster.lib.search.JavaSearchClient;
import com.trackingmaster.lib.search.SearchClient;

import javax.inject.Provider;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
@Module
public final class SearchModule {

    @Singleton
    @Provides
    SearchClient provideSearchClient() {
        return new JavaSearchClient();
    }

    @Provides
    SearchUsecase provideSearchUsecase(SearchClient searchClient) {
        return new AsyncSearchUsecase(searchClient);
    }

    @Singleton
    @Provides
    SearchController provideSearchController(Provider<SearchUsecase> searchUsecaseProvider) {
        return new SearchController(searchUsecaseProvider);
    }
}
