package com.trackingmaster.app.location.injection;

import com.trackingmaster.app.location.ui.LocationFragment;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public interface LocationComponent {
    void inject(LocationFragment fragment);
}
