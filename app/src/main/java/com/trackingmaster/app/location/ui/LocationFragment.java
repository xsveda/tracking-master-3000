package com.trackingmaster.app.location.ui;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.common.base.Optional;
import com.trackingmaster.app.ApplicationComponent;
import com.trackingmaster.app.R;
import com.trackingmaster.app.location.application.LocationController;
import com.trackingmaster.app.location.application.RouteUpdateListener;
import com.trackingmaster.lib.location.Location;

import java.util.List;

import javax.inject.Inject;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.support.v4.content.ContextCompat.checkSelfPermission;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public final class LocationFragment extends MapFragment implements OnMapReadyCallback, RouteUpdateListener {

    // Center of Czechoslovakia
    private static final LatLng DEFAULT_MAP_POSITION = new LatLng(49.3619444, 16.9427778);
    private static final int DEFAULT_MAP_ZOOM = 10;

    @Inject LocationController locationController;

    private Optional<GoogleMap> map = Optional.absent();

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ApplicationComponent.get().inject(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        getMapAsync(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        locationController.register(this);
    }

    @Override
    public void onStop() {
        locationController.unregister(this);
        super.onStop();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        this.map = Optional.of(map);
        moveToDefaultPosition(map);
        updateUi();
    }

    @Override
    public void onRouteUpdate() {
        updateUi();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == R.id.request_permission_location) {
            processPermissionRequestResults(grantResults);
        }
    }

    private void processPermissionRequestResults(int[] grantResults) {
        if (anyGranted(grantResults)) {
            updateUi();
        } else {
            Toast.makeText(getActivity(), R.string.location_permission_not_granted, Toast.LENGTH_LONG).show();
        }
    }

    private boolean anyGranted(int[] grantResults) {
        for (int result : grantResults) {
            if (result == PackageManager.PERMISSION_GRANTED) {
                return true;
            }
        }
        return false;
    }

    private void updateUi() {
        if (map.isPresent()) {
            updateMap(map.get());
        }
    }

    private void updateMap(GoogleMap map) {
        if (isGranted(ACCESS_FINE_LOCATION) || isGranted(ACCESS_COARSE_LOCATION)) {
            setUp(map);
            drawMarkers(map);
        } else {
            requestPermissions(new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION}, R.id.request_permission_location);
        }
    }

    private boolean isGranted(String permission) {
        return checkSelfPermission(getActivity(), permission) == PackageManager.PERMISSION_GRANTED;
    }

    @SuppressWarnings("MissingPermission")
    private void setUp(GoogleMap map) {
        map.setMyLocationEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);
    }

    private void moveToDefaultPosition(GoogleMap map) {
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(DEFAULT_MAP_POSITION, DEFAULT_MAP_ZOOM));
    }

    private void drawMarkers(GoogleMap map) {
        List<Location> points = locationController.getRoute().getPoints();
        for (Location point : points) {
            map.addMarker(new MarkerOptions().position(positionOf(point)));

        }

        if (!points.isEmpty()) {
            Location lastPoint = points.get(points.size() - 1);
            map.animateCamera(CameraUpdateFactory.newLatLng(positionOf(lastPoint)));
        }
    }

    private LatLng positionOf(Location point) {
        return new LatLng(point.getLatitude(), point.getLongitude());
    }
}
