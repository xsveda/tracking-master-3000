package com.trackingmaster.app.location.application;

import com.trackingmaster.app.location.domain.Route;
import com.trackingmaster.lib.location.Location;
import com.trackingmaster.lib.location.LocationClient;
import com.trackingmaster.lib.location.LocationClientListener;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public final class LocationController {

    private final LocationClient locationClient;
    private final LocationClientListener locationClientListener;
    private final Set<RouteUpdateListener> routeListeners = new HashSet<>();
    private Route route = new Route(Collections.<Location>emptyList());

    public LocationController(LocationClient locationClient) {
        this.locationClient = checkNotNull(locationClient);
        this.locationClientListener = new LocationClientListener() {
            @Override
            public void onLocation(Location location) {
                updateRoute(location);
            }
        };
    }

    public void register(RouteUpdateListener listener) {
        checkNotNull(listener);

        if (routeListeners.isEmpty()) {
            locationClient.register(locationClientListener);
        }
        routeListeners.add(listener);
    }

    public void unregister(RouteUpdateListener listener) {
        checkNotNull(listener);
        routeListeners.remove(listener);
    }

    public Route getRoute() {
        return route;
    }

    private void updateRoute(Location location) {
        route = route.withPoint(location);
        notifyRouteUpdateListeners();
    }

    private void notifyRouteUpdateListeners() {
        for (RouteUpdateListener listener : routeListeners) {
            listener.onRouteUpdate();
        }
    }
}
