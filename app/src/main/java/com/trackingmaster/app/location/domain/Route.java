package com.trackingmaster.app.location.domain;

import com.google.common.collect.ImmutableList;
import com.trackingmaster.lib.location.Location;

import java.util.List;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public final class Route {

    private final List<Location> points;

    public Route(List<Location> points) {
        this.points = ImmutableList.copyOf(points);
    }

    public List<Location> getPoints() {
        return points;
    }

    public Route withPoint(Location location) {
        return new Route(ImmutableList.<Location>builder()
                .addAll(getPoints())
                .add(location)
                .build()
        );
    }
}
