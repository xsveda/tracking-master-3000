package com.trackingmaster.app.location.application;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public interface RouteUpdateListener {

    void onRouteUpdate();
}
