package com.trackingmaster.app;

import com.trackingmaster.app.location.injection.LocationComponent;
import com.trackingmaster.app.location.injection.LocationModule;
import com.trackingmaster.app.search.injection.SearchComponent;
import com.trackingmaster.app.search.injection.SearchModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
@Component(modules = {
        SearchModule.class,
        LocationModule.class,
})
@Singleton
public abstract class ApplicationComponent implements SearchComponent, LocationComponent {

    private static ApplicationComponent instance;

    public static ApplicationComponent get() {
        if (instance == null) {
            instance = DaggerApplicationComponent.builder()
                    .locationModule(new LocationModule(TrackingApplication.get()))
                    .build();
        }
        return instance;
    }
}
