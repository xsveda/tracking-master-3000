package com.trackingmaster.app.location.injection;

import android.content.Context;

import com.trackingmaster.app.location.application.LocationController;
import com.trackingmaster.lib.location.LocationClient;
import com.trackingmaster.lib.location.LocationProvider;
import com.trackingmaster.lib.location.provider.fused.FusedLocationProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
@Module
public final class LocationModule {

    private final Context context;

    public LocationModule(Context context) {
        this.context = checkNotNull(context);
    }

    @Singleton
    @Provides
    LocationProvider provideLocationProvider() {
        return new FusedLocationProvider(context);
    }

    @Singleton
    @Provides
    LocationClient provideLocationClient(LocationProvider locationProvider) {
        return new LocationClient(locationProvider);
    }

    @Singleton
    @Provides
    LocationController provideLocationController(LocationClient locationClient) {
        return new LocationController(locationClient);
    }
}
