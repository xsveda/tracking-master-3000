package com.trackingmaster.app.search.application;

import com.trackingmaster.app.search.domain.SearchUsecase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.inject.Provider;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
@RunWith(MockitoJUnitRunner.class)
public final class SearchControllerTest {

    private static final String ANY_QUERY = "ANY_QUERY";
    private static final String ANY_RESULT = "ANY_RESULT";

    @Mock private SearchUsecase searchUsecase;
    @Mock private Provider<SearchUsecase> searchUsecaseProvider;
    @InjectMocks private SearchController controller;

    @Before
    public void setUp() throws Exception {
        when(searchUsecaseProvider.get()).thenReturn(searchUsecase);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNPEWhenProviderIsNullOnConstruct() {
        new SearchController(null);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNPEForNullListenerOnRegister() {
        controller.register(null);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNPEForNullListenerOnUnregister() {
        controller.unregister(null);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNPEWhenInputIsNullOnSearch() {
        controller.search(null);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNPEWhenSearchResultIsNullOnSearchResult() {
        controller.onSearchResult(null);
    }

    @Test
    public void shouldDelegateCallToNewlyCreatedUsecaseOnSearch() {
        controller.search(ANY_QUERY);

        verify(searchUsecaseProvider).get();
        verify(searchUsecase).execute(ANY_QUERY, controller);
    }

    @Test
    public void shouldReturnFalseWhenNoSearchStartedOnIsRunning() {
        boolean result = controller.isRunning();

        assertThat(result, is(false));
    }

    @Test
    public void shouldReturnTrueWhenSearchIsStartedOnIsRunning() {
        controller.search(ANY_QUERY);

        boolean result = controller.isRunning();

        assertThat(result, is(true));
    }

    @Test
    public void shouldReturnFalseWhenSearchIsStartedAndFinishedOnIsRunning() {
        controller.search(ANY_QUERY);
        controller.onSearchResult(ANY_RESULT);

        boolean result = controller.isRunning();

        assertThat(result, is(false));
    }

    @Test
    public void shouldReturnLastQueryAfterSearch() {
        assertThat(controller.getLastQuery().isPresent(), is(false));

        controller.search(ANY_QUERY);

        assertThat(controller.getLastQuery().isPresent(), is(true));
        assertThat(controller.getLastQuery().get(), is(ANY_QUERY));
    }

    @Test
    public void shouldReturnLastResultAfterSearch() {
        assertThat(controller.getLastResult().isPresent(), is(false));

        controller.search(ANY_QUERY);
        controller.onSearchResult(ANY_RESULT);

        assertThat(controller.getLastResult().isPresent(), is(true));
        assertThat(controller.getLastResult().get(), is(ANY_RESULT));
    }

    @Test
    public void shouldClearPreviousLastResultAfterNewSearchStarted() {
        controller.search(ANY_QUERY);
        controller.onSearchResult(ANY_RESULT);
        controller.search(ANY_QUERY);

        assertThat(controller.getLastResult().isPresent(), is(false));
    }
}
