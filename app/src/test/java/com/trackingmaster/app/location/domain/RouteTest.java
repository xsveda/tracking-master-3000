package com.trackingmaster.app.location.domain;

import com.trackingmaster.lib.location.Location;

import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsEmptyCollection.emptyCollectionOf;
import static org.junit.Assert.assertThat;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public final class RouteTest {

    private static final Location ANY_LOCATION = new Location(0, 0);

    @Test(expected = NullPointerException.class)
    public void shouldThrowNPEForNullPointsOnConstruction() {
        new Route(null);
    }

    @Test
    public void shouldAllowToCreateRouteFromNoPoints() {
        Route route = emptyRoute();

        List<Location> points = route.getPoints();

        assertThat(points, emptyCollectionOf(Location.class));
    }

    @Test
    public void shouldBeResistantToOriginalListModification() {
        List<Location> original = newArrayList(ANY_LOCATION, ANY_LOCATION);

        Route route = new Route(original);
        assertThat(route.getPoints(), hasSize(2));

        original.add(ANY_LOCATION);
        assertThat(original, hasSize(3));
        assertThat(route.getPoints(), hasSize(2));
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNPEForNullLocationOnWithPoint() {
        emptyRoute().withPoint(null);
    }

    @Test
    public void shouldCreateNewRouteWithAdditionalPointOnWithPoint() {
        Route original = emptyRoute();

        Route newRoute = original.withPoint(ANY_LOCATION);

        assertThat(original.getPoints(), emptyCollectionOf(Location.class));
        assertThat(newRoute.getPoints(), hasSize(1));
    }

    private Route emptyRoute() {
        return new Route(Collections.<Location>emptyList());
    }
}

