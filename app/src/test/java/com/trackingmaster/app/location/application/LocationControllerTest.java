package com.trackingmaster.app.location.application;

import com.trackingmaster.lib.location.Location;
import com.trackingmaster.lib.location.LocationClient;
import com.trackingmaster.lib.location.LocationClientListener;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public final class LocationControllerTest {

    private static final RouteUpdateListener ANY_LISTENER = mock(RouteUpdateListener.class);
    private static final Location LOCATION_1 = new Location(1, 1);
    private static final Location LOCATION_2 = new Location(2, 2);
    private static final Location LOCATION_3 = new Location(3, 3);

    private LocationClient locationClient = mock(LocationClient.class);
    private LocationController controller = new LocationController(locationClient);


    @Test(expected = NullPointerException.class)
    public void shouldThrowNPEForNullClientOnConstruction() {
        new LocationController(null);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNPEForNullListenerOnRegister() {
        controller.register(null);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNPEForNullListenerOnUnregister() {
        controller.unregister(null);
    }

    @Test
    public void shouldNotRegisterToClientWhenNoListenerRegisteredToController() {
        verifyZeroInteractions(locationClient);
    }

    @Test
    public void shouldRegisterToClientWhenListenerRegisteredToController() {
        controller.register(ANY_LISTENER);

        verify(locationClient).register(any(LocationClientListener.class));
    }

    @Test
    public void shouldRegisterToClientWhenListenerRegisteredToControllerJustOnce() {
        controller.register(ANY_LISTENER);
        controller.register(ANY_LISTENER);
        controller.register(ANY_LISTENER);

        verify(locationClient).register(any(LocationClientListener.class));
    }

    @Test
    public void shouldNeverUnregisterFromClientWhenListenerUnregisteredFromController() {
        controller.register(ANY_LISTENER);
        controller.unregister(ANY_LISTENER);

        verify(locationClient, never()).unregister(any(LocationClientListener.class));
    }

    @Test
    public void shouldNotifyAboutUpdatedRouteAfterEveryLocationUpdateEmittedByLocationClient() {
        RouteUpdateListener routeUpdateListener = mock(RouteUpdateListener.class);
        LocationClientListener controllerLocationListener = bindControllerToLocationClient(routeUpdateListener);

        controllerLocationListener.onLocation(LOCATION_1);
        verifyRoute(routeUpdateListener, LOCATION_1);

        controllerLocationListener.onLocation(LOCATION_2);
        verifyRoute(routeUpdateListener, LOCATION_1, LOCATION_2);

        controllerLocationListener.onLocation(LOCATION_3);
        verifyRoute(routeUpdateListener, LOCATION_1, LOCATION_2, LOCATION_3);
    }

    @Test
    public void shouldContinueBuildingRouteEvenWhenNoRouteListenerRegistered() {
        RouteUpdateListener routeUpdateListener = mock(RouteUpdateListener.class);
        LocationClientListener controllerLocationListener = bindControllerToLocationClient(routeUpdateListener);

        controllerLocationListener.onLocation(LOCATION_1);
        verifyRoute(routeUpdateListener, LOCATION_1);

        controller.unregister(routeUpdateListener);
        controllerLocationListener.onLocation(LOCATION_2);
        controller.register(routeUpdateListener);

        controllerLocationListener.onLocation(LOCATION_3);
        verifyRoute(routeUpdateListener, LOCATION_1, LOCATION_2, LOCATION_3);
    }

    private void verifyRoute(RouteUpdateListener routeUpdateListener, Location... locations) {
        verify(routeUpdateListener).onRouteUpdate();
        reset(routeUpdateListener);
        assertThat(controller.getRoute().getPoints(), hasItems(locations));
    }

    private LocationClientListener bindControllerToLocationClient(RouteUpdateListener routeUpdateListener) {
        controller.register(routeUpdateListener);
        ArgumentCaptor<LocationClientListener> locationClientListenerCaptor = ArgumentCaptor.forClass(LocationClientListener.class);
        verify(locationClient).register(locationClientListenerCaptor.capture());
        return locationClientListenerCaptor.getValue();
    }
}
