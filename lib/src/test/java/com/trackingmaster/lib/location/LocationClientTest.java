package com.trackingmaster.lib.location;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public final class LocationClientTest {

    private static final Location ANY_LOCATION = new Location(42, -42);
    private static final LocationClientListener ANY_LISTENER = mock(LocationClientListener.class);

    private LocationProvider provider = mock(LocationProvider.class);
    private LocationClient client = new LocationClient(provider);

    @Test(expected = NullPointerException.class)
    public void shouldThrowNPEForNullLocationProvider() {
        new LocationClient(null);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNPEForNullListenerOnRegister() {
        client.register(null);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNPEForNullListenerOnUnregister() {
        client.unregister(null);
    }

    @Test
    public void shouldNotRegisterToProviderWhenNoListenerRegisteredToClient() {
        verifyZeroInteractions(provider);
    }

    @Test
    public void shouldRegisterToProviderWhenListenerRegisteredToClient() {
        client.register(ANY_LISTENER);

        verify(provider).register(any(LocationProviderListener.class));
    }

    @Test
    public void shouldRegisterToProviderWhenListenerRegisteredToClientJustOnce() {
        client.register(ANY_LISTENER);
        client.register(ANY_LISTENER);
        client.register(ANY_LISTENER);

        verify(provider).register(any(LocationProviderListener.class));
    }

    @Test
    public void shouldUnregisterFromProviderWhenListenerUnregisteredFromClient() {
        client.register(ANY_LISTENER);
        client.unregister(ANY_LISTENER);

        verify(provider).unregister(any(LocationProviderListener.class));
    }

    @Test
    public void shouldUnregisterFromProviderWhenListenerUnregisteredFromClientOnlyWhenAllListenersUnregistered() {
        client.register(mock(LocationClientListener.class));
        client.register(ANY_LISTENER);
        client.unregister(ANY_LISTENER);

        verify(provider, never()).unregister(any(LocationProviderListener.class));
    }

    @Test
    public void shouldPassLocationFromProviderToAllRegisteredListeners() {
        LocationClientListener listener1 = mock(LocationClientListener.class);
        LocationClientListener listener2 = mock(LocationClientListener.class);
        client.register(listener1);
        client.register(listener2);
        ArgumentCaptor<LocationProviderListener> providerListenerCaptor = ArgumentCaptor.forClass(LocationProviderListener.class);
        verify(provider).register(providerListenerCaptor.capture());

        providerListenerCaptor.getValue().onLocation(ANY_LOCATION);

        verify(listener1).onLocation(ANY_LOCATION);
        verify(listener2).onLocation(ANY_LOCATION);
    }
}
