package com.trackingmaster.lib.search;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public final class JavaSearchClientTest {

    private SearchClient client = new JavaSearchClient();

    @Test(expected = NullPointerException.class)
    public void shouldThrowNPEWhenInputIsNullOnSearch() {
        client.search(null);
    }

    @Test
    public void shouldReturnEmptyStringForEmptyInputOnSearch() {
        String empty = "";

        String result = client.search(empty);

        assertThat(result, is(empty));
    }

    @Test
    public void shouldDuplicateNonEmptyInputOnSearch() {
        String input = "Some dummy text";

        String result = client.search(input);

        assertThat(result, is(input + input));
    }
}
