package com.trackingmaster.lib.location;

/**
 * Listener called by {@link LocationProvider} on location update.
 *
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public interface LocationProviderListener {

    void onLocation(Location location);
}
