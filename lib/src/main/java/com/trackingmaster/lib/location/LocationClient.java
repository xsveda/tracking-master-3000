package com.trackingmaster.lib.location;

import java.util.HashSet;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public /*final*/ class LocationClient {

    private final LocationProvider provider;
    private final LocationProviderListener providerListener;
    private final Set<LocationClientListener> clientListeners = new HashSet<>();

    public LocationClient(LocationProvider provider) {
        this.provider = checkNotNull(provider);
        this.providerListener = new LocationProviderListener() {
            @Override
            public void onLocation(Location location) {
                notifyRegisteredListeners(location);
            }
        };
    }

    public void register(LocationClientListener listener) {
        checkNotNull(listener);

        if (clientListeners.isEmpty()) {
            provider.register(providerListener);
        }
        clientListeners.add(listener);

    }

    public void unregister(LocationClientListener listener) {
        checkNotNull(listener);

        clientListeners.remove(listener);
        if (clientListeners.isEmpty()) {
            provider.unregister(providerListener);
        }
    }

    private void notifyRegisteredListeners(Location location) {
        for (LocationClientListener listener : clientListeners) {
            listener.onLocation(location);
        }
    }
}
