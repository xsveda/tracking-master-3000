package com.trackingmaster.lib.location.provider.fused;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.trackingmaster.lib.R;
import com.trackingmaster.lib.location.LocationProvider;
import com.trackingmaster.lib.location.LocationProviderListener;

import java.util.HashSet;
import java.util.Set;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.support.v4.content.ContextCompat.checkSelfPermission;
import static com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public final class FusedLocationProvider implements LocationProvider, ConnectionCallbacks, LocationListener {

    private static final long LOCATION_UPDATE_INTERVAL = SECONDS.toMillis(2);

    private final Context context;
    private final GoogleApiClient googleApiClient;
    private final Set<LocationProviderListener> providerListeners = new HashSet<>();

    public FusedLocationProvider(Context context) {
        this.context = checkNotNull(context);
        this.googleApiClient = new GoogleApiClient.Builder(this.context)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void register(LocationProviderListener listener) {
        checkNotNull(listener);

        if (!googleApiClient.isConnected()) {
            googleApiClient.connect();
        }
        providerListeners.add(listener);
    }

    @Override
    public void unregister(LocationProviderListener listener) {
        checkNotNull(listener);

        providerListeners.remove(listener);
        if (providerListeners.isEmpty()) {
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (isGranted(ACCESS_FINE_LOCATION) || isGranted(ACCESS_COARSE_LOCATION)) {
            requestLocationUpdates();
        } else {
            Toast.makeText(context, R.string.error_missing_location_permission, Toast.LENGTH_LONG).show();
        }
    }

    @SuppressWarnings("MissingPermission")
    private void requestLocationUpdates() {
        LocationRequest request = new LocationRequest();
        request.setInterval(LOCATION_UPDATE_INTERVAL);
        request.setFastestInterval(LOCATION_UPDATE_INTERVAL);
        request.setPriority(PRIORITY_HIGH_ACCURACY);
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, request, this);
    }

    @Override
    public void onConnectionSuspended(int reason) {
    }

    @Override
    public void onLocationChanged(Location location) {
        notifyRegisteredListeners(new com.trackingmaster.lib.location.Location(
                location.getLatitude(),
                location.getLongitude()
        ));
    }

    private boolean isGranted(String permission) {
        return checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    private void notifyRegisteredListeners(com.trackingmaster.lib.location.Location location) {
        for (LocationProviderListener listener : providerListeners) {
            listener.onLocation(location);
        }
    }
}
