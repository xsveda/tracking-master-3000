package com.trackingmaster.lib.location.provider.fake;

import android.os.Handler;
import android.os.Looper;

import com.google.common.collect.Lists;
import com.trackingmaster.lib.location.Location;
import com.trackingmaster.lib.location.LocationProvider;
import com.trackingmaster.lib.location.LocationProviderListener;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public final class FakeLocationProvider implements LocationProvider {

    private static final long NEXT_EMIT_DELAY = 1200L;
    private static final List<Location> FAKE_DATA = Lists.newArrayList(
            new Location(49.250, 16.550),
            new Location(49.250, 16.650),
            new Location(49.220, 16.600),
            new Location(49.210, 16.600),
            new Location(49.150, 16.540),
            new Location(49.140, 16.570),
            new Location(49.135, 16.600),
            new Location(49.140, 16.630),
            new Location(49.150, 16.660)
    );

    private final Set<LocationProviderListener> providerListeners = new HashSet<>();
    private final Handler handler;
    private int nextFake = 0;

    public FakeLocationProvider() {
        this.handler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void register(LocationProviderListener listener) {
        checkNotNull(listener);
        providerListeners.add(listener);
        scheduleFakeDataEmit();
    }

    @Override
    public void unregister(LocationProviderListener listener) {
        checkNotNull(listener);
        providerListeners.remove(listener);
    }

    private void scheduleFakeDataEmit() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                emitFakeData();
            }
        }, NEXT_EMIT_DELAY);
    }

    private void emitFakeData() {
        if (FAKE_DATA.size() > nextFake) {
            Location location = FAKE_DATA.get(nextFake);
            notifyRegisteredListeners(location);
            nextFake++;
            scheduleFakeDataEmit();
        }
    }

    private void notifyRegisteredListeners(Location location) {
        for (LocationProviderListener listener : providerListeners) {
            listener.onLocation(location);
        }
    }
}
