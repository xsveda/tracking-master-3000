package com.trackingmaster.lib.location;

/**
 * Listener called by {@link LocationClient} on location update.
 *
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public interface LocationClientListener {

    void onLocation(Location location);
}
