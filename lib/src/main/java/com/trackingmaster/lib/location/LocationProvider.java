package com.trackingmaster.lib.location;

/**
 * Location data provider for {@link LocationClient}.
 *
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public interface LocationProvider {

    void register(LocationProviderListener listener);

    void unregister(LocationProviderListener listener);
}
