package com.trackingmaster.lib.search;

import java.util.Random;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public final class JavaSearchClient implements SearchClient {

    private final Random random = new Random();

    @Override
    public String search(String searchQuery) {
        checkNotNull(searchQuery);

        // Simulate extensive work
        try {
            Thread.sleep(random.nextInt(1000) + 500); // Range: [500, 1500) ms
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }

        return searchQuery + searchQuery;
    }
}
