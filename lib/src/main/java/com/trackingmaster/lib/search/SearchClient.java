package com.trackingmaster.lib.search;

/**
 * @author Pavel Sveda <xsveda@gmail.com>
 */
public interface SearchClient {
    String search(String searchQuery);
}
