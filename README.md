# Tracking Master 3000 #

### Project structure ###

* Project is separated to APP module and LIB module
* Each module is separated per feature - Search x Location
* In app module every feature is separated to layers. The golden rule is that lower layer is not dependent on any higher layer. 
* Order of layers from lowest: Domain, Application, UI/Data Access (last two are on the same level)
* The project instances dependency tree is build using Dagger dependency inject framework.

### Search feature ###

##### Library module #####

Feature entry point is a [SearchClient](/lib/src/main/java/com/trackingmaster/lib/search/SearchClient.java).
Library contains also single implementation [JavaSearchClient](/lib/src/main/java/com/trackingmaster/lib/search/JavaSearchClient.java) that simulates and extensive work that should be done on background and returns concatenated input query with itself.

##### Application module #####

The `SearchClient` is used by an [AsyncSearchUsecase](/app/src/main/java/com/trackingmaster/app/search/dataaccess/AsyncSearchUsecase.java) which is an asynchronous implementation of [SearchUsecase](/app/src/main/java/com/trackingmaster/app/search/domain/SearchUsecase.java). The search tasks execution is driven by [SearchController](/app/src/main/java/com/trackingmaster/app/search/application/SearchController.java) where the current feature state is cached. The state is then presented by [SearchFragment](/app/src/main/java/com/trackingmaster/app/search/ui/SearchFragment.java).

### Location feature ###

##### Library module #####

Feature entry point is a [LocationClient](/lib/src/main/java/com/trackingmaster/lib/location/LocationClient.java). The client itself is just a wrapper around [LocationProvider](/lib/src/main/java/com/trackingmaster/lib/location/LocationProvider.java) that is to be provided in constructor.
The library itself contains also two `LocationProvider` implementations:

* [FusedLocationProvider](/lib/src/main/java/com/trackingmaster/lib/location/provider/fused/FusedLocationProvider.java): based on Google Play Services' Fused Location API.
* [FakeLocationProvider](/lib/src/main/java/com/trackingmaster/lib/location/provider/fake/FakeLocationProvider.java): provides fake data to showcase the feature without need to simulate device location change.

##### Application module #####

[LocationController](/app/src/main/java/com/trackingmaster/app/location/application/LocationController.java) is responsible for binding to a `LocationClient` and caches all the location points emitted to a [Route](/app/src/main/java/com/trackingmaster/app/location/domain/Route.java). The current route is then presented by [LocationFragment](/app/src/main/java/com/trackingmaster/app/location/ui/LocationFragment.java).

### Project build ###

The project build has a single build flavor where a type of `LocationProvider` is selected:

* `fusedLocation` for [FusedLocationProvider](/lib/src/main/java/com/trackingmaster/lib/location/provider/fused/FusedLocationProvider.java)
* `fakeLocation` for [FakeLocationProvider](/lib/src/main/java/com/trackingmaster/lib/location/provider/fake/FakeLocationProvider.java)